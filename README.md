2048 Game
----------

New game will start once the app is open and a scoreboard will be displayed up top.

To play: swipe left, right, up or down.

The game is over when you get a 2048 tile or there are no more moves. A notification covering the game board will let you know if you won or lost. To resume a new game after the game is over, simply swipe in any direction on the game board.