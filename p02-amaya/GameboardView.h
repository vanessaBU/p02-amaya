//
//  GameboardView.h
//  p02-amaya
//
//  Created by Vanessa Amaya on 1/31/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#ifndef GameboardView_h
#define GameboardView_h

#import <UIKit/UIKit.h>

#include "TileView.h"

@class GameboardView;
@interface GameboardView : UIView

typedef struct matrixCoordinate {
    NSUInteger row, col;
} matrixCoordinate;

@property (nonatomic) CGRect frame;
@property (nonatomic) CGFloat tileSize;
@property (nonatomic) CGFloat padding;
@property (nonatomic) BOOL gameOver;
@property (nonatomic) BOOL gameWon;
@property (nonatomic, strong) UILabel *backgroundLabel;
@property (nonatomic) NSMutableArray *allTiles;
@property (nonatomic) NSMutableArray *emptyTiles;
@property (nonatomic) NSUInteger score;

- (id)initWithFrame:(CGRect)frame
           tileSize:(CGFloat)width
            padding:(CGFloat)pad;
- (void)insertTile;
- (void)resetTiles; // insert all empty tiles into gameboard & add view coordinates to empty tiles list
- (CGFloat)getViewCoord:(CGFloat)index; // returns the position in the view (x, y) when given a row or column in the grid, calculated based on tile width and padding values
- (matrixCoordinate)getMatrixCoord:(NSUInteger)index; // returns the row and column in the grid (ie, 0 - 3) converted from a 1d array index
- (NSUInteger)getIndex:(matrixCoordinate)matrixCoord; // returns 1d array index from a matrix coordinate
- (NSUInteger)getIndex:(NSUInteger)row :(NSUInteger)column;

- (void)setTileValue:(NSUInteger)row :(NSUInteger)column :(NSUInteger)value;
- (void)setTileValue:(NSUInteger)index :(NSUInteger)value;
- (void)doubleTileValue:(NSUInteger)row :(NSUInteger)column;
- (void)doubleTileValue:(NSUInteger)index;

- (void)shiftLeft;
- (void)shiftRight;
- (void)shiftUp;
- (void)shiftDown;

- (void)alertGameOver;
- (void)alertGameWon;

@end

#endif /* GameboardView_h */
