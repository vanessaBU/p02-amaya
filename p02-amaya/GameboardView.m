//
//  GameboardView.m
//  p02-amaya
//
//  Created by Vanessa Amaya on 1/31/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameboardView.h"

@interface GameboardView ()

@end

@implementation GameboardView

- (id)initWithFrame:(CGRect)frame
           tileSize:(CGFloat)size
            padding:(CGFloat)pad
{
    self = [super initWithFrame:frame];
    if (self) {
        _frame = frame;
        _tileSize = _frame.size.width / 5.0;
        _padding = 0.2 * _tileSize;
        _allTiles = [[NSMutableArray alloc] init];
        _emptyTiles = [[NSMutableArray alloc] init];
        _score = 0;
        [self resetTiles];
    }
    return self;
}

- (void)insertTile {
    
    // game over if no empty tiles left
    if (_emptyTiles.count == 0) {
        _gameOver = true;
        [self alertGameOver];
    }
    else {
        // stackoverflow.com/questions/9678373/generate-random-numbers-between-two-numbers-in-objective-c
        // random = lower + arc4random() % (upper - lower + 1)
        int randomValue = (1 + arc4random() % 2) * 2;
        int randomIndex = arc4random() % (_emptyTiles.count);
        TileView *tv = _emptyTiles[randomIndex];
        [tv setTileValue:randomValue];
        [_emptyTiles removeObjectAtIndex:randomIndex];
    }
}

- (void)resetTiles {
    
    // start new game
    _gameOver = _gameWon = false;
    _score = 0;
    
    // remove subviews
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    // create base of gameboard as subview
    _backgroundLabel = [[UILabel alloc] initWithFrame:_frame];
    _backgroundLabel.backgroundColor = [UIColor colorWithRed:0. green:0. blue:0. alpha:0.25];
    [self addSubview:_backgroundLabel];
    
    // clear out all tiles on the board
    if (_allTiles.count != 0) {
        [_allTiles removeAllObjects];
    }
    // delete all tiles in empty tiles array
    if (_emptyTiles.count != 0) {
        [_emptyTiles removeAllObjects];
    }
    // insert empty tiles into each slot on the gameboard
    for (int i = 0; i < 4; i++) {
        CGFloat yCoord = [self getViewCoord:i];
        for (int j = 0; j < 4; j++) {
            CGFloat xCoord = [self getViewCoord:j];
            TileView *tv = [[TileView alloc] initWithFrame:CGRectMake(xCoord, yCoord, _tileSize, _tileSize) tileValue:0];
            [self addSubview:tv];
            [_allTiles addObject:tv];
            [_emptyTiles addObject:tv];
        }
    }
    // insert 2 random tiles to start the game
    [self insertTile];
    [self insertTile];
}

- (CGFloat)getViewCoord:(CGFloat)index {
    CGFloat pad = (index + 1.) * (_padding / 2.);
    CGFloat offset = index * (_tileSize / 2.);
    return pad + offset;
}

- (matrixCoordinate)getMatrixCoord:(NSUInteger)index {
    NSUInteger col = index % 4;
    NSUInteger row = (index - col) / 4;
    matrixCoordinate mc;
    mc.row = row;
    mc.col = col;
    return mc;
}

- (NSUInteger)getIndex:(matrixCoordinate)matrixCoord {
    return (matrixCoord.row * 4 + matrixCoord.col);
}

- (NSUInteger)getIndex:(NSUInteger)row :(NSUInteger)column {
    return (row * 4 + column);
}

- (void)setTileValue:(NSUInteger)row :(NSUInteger)column :(NSUInteger)value {
    NSUInteger idx = [self getIndex:row :column];
    TileView *tv = _allTiles[idx];
    [tv setTileValue:value];
}

- (void)setTileValue:(NSUInteger)index :(NSUInteger)value {
    TileView *tv = _allTiles[index];
    [tv setTileValue:value];
}

- (void)doubleTileValue:(NSUInteger)row :(NSUInteger)column {
    NSUInteger idx = [self getIndex:row :column];
    TileView *tv = _allTiles[idx];
    [tv doubleTileValue];
}

- (void)doubleTileValue:(NSUInteger)index {
    TileView *tv = _allTiles[index];
    [tv doubleTileValue];
}

- (void)shiftLeft {
    
    for (int m = 0; m < 4; m++) {
        for (int n = 0; n < 3; n++) {
            
            NSUInteger myIndex = [self getIndex:m :n];
            NSUInteger myValue = [(TileView *)_allTiles[myIndex] value];
            
            // shift until my tile is not empty
            if (myValue == 0) {
                
                int it = n + 1; // set iterator to next
                bool isEmpty = true; // keep track of whether me is empty
                while (it < 4 && isEmpty) {
                    
                    NSUInteger nextIndex = [self getIndex:m :it];
                    NSUInteger nextValue = [(TileView *)_allTiles[nextIndex] value];
                    if (nextValue != 0) {
                        
                        // set my tile value to next tile value & remove self from empty tiles list
                        [_allTiles[myIndex] setTileValue:nextValue];
                        TileView *myTile = (TileView *)_allTiles[myIndex];
                        [_emptyTiles removeObjectIdenticalTo:myTile];
                        
                        // set next tile to empty & add to empty tiles list
                        [_allTiles[nextIndex] setTileValue:0];
                        TileView *nextTile = (TileView *)_allTiles[nextIndex];
                        [_emptyTiles addObject:nextTile];
                        
                        // update my value & set empty to false to break out of loop
                        myValue = nextValue;
                        isEmpty = false;
                    }
                    it++; // set iterator to next next tile
                }
            }
            // if after shifting, the value is still zero, go to the next row
            if (myValue == 0) {
                break; // break out of inner loop iterating through columns
            }
            // otherwise, try and find a match for my value
            else {
                
                int it = n + 1; // set iterator to next
                bool nextEmpty = true; // loop while next tile is empty
                while (it < 4 && nextEmpty) {
                    
                    NSUInteger nextIndex = [self getIndex:m :it];
                    NSUInteger nextValue = [(TileView *)_allTiles[nextIndex] value];
                    nextEmpty = (nextValue == 0);
                    if (nextValue == myValue) {
                        
                        // double my tile value
                        [_allTiles[myIndex] doubleTileValue];
                        
                        // set next tile to empty & add to empty tiles list
                        [_allTiles[nextIndex] setTileValue:0];
                        TileView *nextTile = (TileView *)_allTiles[nextIndex];
                        [_emptyTiles addObject:nextTile];
                        
                        // update my value
                        myValue *= 2;
                        _score += myValue;
                        
                        // check if game won
                        if (myValue == 2048) {
                            _gameWon = _gameOver = true;
                            [self alertGameWon];
                            return;
                        }
                    }
                    it++; // set iterator to next next tile
                }
            }
        }
    }
    // insert random tile after each shift
    [self performSelector:@selector(insertTile) withObject:nil afterDelay:0.25];
}

- (void)shiftRight {
    
    for (int m = 3; m >= 0; m--) {
        for (int n = 3; n > 0; n--) {
            
            NSUInteger myIndex = [self getIndex:m :n];
            NSUInteger myValue = [(TileView *)_allTiles[myIndex] value];
            
            // shift until my tile is not empty
            if (myValue == 0) {
                
                int it = n - 1; // set iterator to next
                bool isEmpty = true; // keep track of whether me is empty
                while (it >= 0 && isEmpty) {
                    
                    NSUInteger nextIndex = [self getIndex:m :it];
                    NSUInteger nextValue = [(TileView *)_allTiles[nextIndex] value];
                    if (nextValue != 0) {
                        
                        // set my tile value to next tile value & remove self from empty tiles list
                        [_allTiles[myIndex] setTileValue:nextValue];
                        TileView *myTile = (TileView *)_allTiles[myIndex];
                        [_emptyTiles removeObjectIdenticalTo:myTile];
                        
                        // set next tile to empty & add to empty tiles list
                        [_allTiles[nextIndex] setTileValue:0];
                        TileView *nextTile = (TileView *)_allTiles[nextIndex];
                        [_emptyTiles addObject:nextTile];
                        
                        // update my value & set empty to false to break out of loop
                        myValue = nextValue;
                        isEmpty = false;
                    }
                    it--; // set iterator to next next tile
                }
            }
            // if after shifting, the value is still zero, go to the next row
            if (myValue == 0) {
                break; // break out of inner loop iterating through columns
            }
            // otherwise, try and find a match for my value
            else {
                
                int it = n - 1; // set iterator to next
                bool nextEmpty = true; // loop while next tile is empty
                while (it >= 0 && nextEmpty) {
                    
                    NSUInteger nextIndex = [self getIndex:m :it];
                    NSUInteger nextValue = [(TileView *)_allTiles[nextIndex] value];
                    nextEmpty = (nextValue == 0);
                    if (nextValue == myValue) {
                        
                        // double my tile value
                        [_allTiles[myIndex] doubleTileValue];
                        
                        // set next tile to empty & add to empty tiles list
                        [_allTiles[nextIndex] setTileValue:0];
                        TileView *nextTile = (TileView *)_allTiles[nextIndex];
                        [_emptyTiles addObject:nextTile];
                        
                        // update my value
                        myValue *= 2;
                        _score += myValue;
                        
                        // check if game won
                        if (myValue == 2048) {
                            _gameWon = _gameOver = true;
                            [self alertGameWon];
                            return;
                        }
                    }
                    it--; // set iterator to next next tile
                }
            }
        }
    }
    // insert random tile after each shift
    [self performSelector:@selector(insertTile) withObject:nil afterDelay:0.25];
}

- (void)shiftUp {
    
    for (int n = 0; n < 4; n++) {
        for (int m = 0; m < 3; m++) {
            
            NSUInteger myIndex = [self getIndex:m :n];
            NSUInteger myValue = [(TileView *)_allTiles[myIndex] value];
            
            // shift until my tile is not empty
            if (myValue == 0) {
                
                int it = m + 1; // set iterator to next
                bool isEmpty = true; // keep track of whether me is empty
                while (it < 4 && isEmpty) {
                    
                    NSUInteger nextIndex = [self getIndex:it :n];
                    NSUInteger nextValue = [(TileView *)_allTiles[nextIndex] value];
                    if (nextValue != 0) {
                        
                        // set my tile value to next tile value & remove self from empty tiles list
                        [_allTiles[myIndex] setTileValue:nextValue];
                        TileView *myTile = (TileView *)_allTiles[myIndex];
                        [_emptyTiles removeObjectIdenticalTo:myTile];
                        
                        // set next tile to empty & add to empty tiles list
                        [_allTiles[nextIndex] setTileValue:0];
                        TileView *nextTile = (TileView *)_allTiles[nextIndex];
                        [_emptyTiles addObject:nextTile];
                        
                        // update my value & set empty to false to break out of loop
                        myValue = nextValue;
                        isEmpty = false;
                    }
                    it++; // set iterator to next next tile
                }
            }
            // if after shifting, the value is still zero, go to the next row
            if (myValue == 0) {
                break; // break out of inner loop iterating through columns
            }
            // otherwise, try and find a match for my value
            else {
                
                int it = m + 1; // set iterator to next
                bool nextEmpty = true; // loop while next tile is empty
                while (it < 4 && nextEmpty) {
                    
                    NSUInteger nextIndex = [self getIndex:it :n];
                    NSUInteger nextValue = [(TileView *)_allTiles[nextIndex] value];
                    nextEmpty = (nextValue == 0);
                    if (nextValue == myValue) {
                        
                        // double my tile value
                        [_allTiles[myIndex] doubleTileValue];
                        
                        // set next tile to empty & add to empty tiles list
                        [_allTiles[nextIndex] setTileValue:0];
                        TileView *nextTile = (TileView *)_allTiles[nextIndex];
                        [_emptyTiles addObject:nextTile];
                        
                        // update my value
                        myValue *= 2;
                        _score += myValue;
                        
                        // check if game won
                        if (myValue == 2048) {
                            _gameWon = _gameOver = true;
                            [self alertGameWon];
                            return;
                        }
                    }
                    it++; // set iterator to next next tile
                }
            }
        }
    }
    // insert random tile after each shift
    [self performSelector:@selector(insertTile) withObject:nil afterDelay:0.25];
}

- (void)shiftDown {
    
    for (int n = 3; n >= 0; n--) {
        for (int m = 3; m > 0; m--) {
            
            NSUInteger myIndex = [self getIndex:m :n];
            NSUInteger myValue = [(TileView *)_allTiles[myIndex] value];
            
            // shift until my tile is not empty
            if (myValue == 0) {
                
                int it = m - 1; // set iterator to next
                bool isEmpty = true; // keep track of whether me is empty
                while (it >= 0 && isEmpty) {
                    
                    NSUInteger nextIndex = [self getIndex:it :n];
                    NSUInteger nextValue = [(TileView *)_allTiles[nextIndex] value];
                    if (nextValue != 0) {
                        
                        // set my tile value to next tile value & remove self from empty tiles list
                        [_allTiles[myIndex] setTileValue:nextValue];
                        TileView *myTile = (TileView *)_allTiles[myIndex];
                        [_emptyTiles removeObjectIdenticalTo:myTile];
                        
                        // set next tile to empty & add to empty tiles list
                        [_allTiles[nextIndex] setTileValue:0];
                        TileView *nextTile = (TileView *)_allTiles[nextIndex];
                        [_emptyTiles addObject:nextTile];
                        
                        // update my value & set empty to false to break out of loop
                        myValue = nextValue;
                        isEmpty = false;
                    }
                    it--; // set iterator to next next tile
                }
            }
            // if after shifting, the value is still zero, go to the next row
            if (myValue == 0) {
                break; // break out of inner loop iterating through columns
            }
            // otherwise, try and find a match for my value
            else {
                
                int it = m - 1; // set iterator to next
                bool nextEmpty = true; // loop while next tile is empty
                while (it >= 0 && nextEmpty) {
                    
                    NSUInteger nextIndex = [self getIndex:it :n];
                    NSUInteger nextValue = [(TileView *)_allTiles[nextIndex] value];
                    nextEmpty = (nextValue == 0);
                    if (nextValue == myValue) {
                        
                        // double my tile value
                        [_allTiles[myIndex] doubleTileValue];
                        
                        // set next tile to empty & add to empty tiles list
                        [_allTiles[nextIndex] setTileValue:0];
                        TileView *nextTile = (TileView *)_allTiles[nextIndex];
                        [_emptyTiles addObject:nextTile];
                        
                        // update my value
                        myValue *= 2;
                        _score += myValue;
                        
                        // check if game won
                        if (myValue == 2048) {
                            _gameWon = _gameOver = true;
                            [self alertGameWon];
                            return;
                        }
                    }
                    it--; // set iterator to next next tile
                }
            }
        }
    }
    // insert random tile after each shift
    [self performSelector:@selector(insertTile) withObject:nil afterDelay:0.25];
}

- (void)alertGameOver {
    if (_gameOver && !_gameWon) {
        UILabel *gameOverLabel = [[UILabel alloc] initWithFrame:_frame];
        gameOverLabel.backgroundColor = [UIColor colorWithRed:255. green:128. blue:64. alpha:0.4];
        gameOverLabel.text = @"Game Over!";
        gameOverLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:gameOverLabel];
        return;
    }
}

- (void)alertGameWon {
    if (_gameOver && _gameWon) {
        UILabel *gameWonLabel = [[UILabel alloc] initWithFrame:_frame];
        gameWonLabel.backgroundColor = [UIColor colorWithRed:64. green:255. blue:128. alpha:0.4];
        gameWonLabel.text = @"You win!";
        gameWonLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:gameWonLabel];
        return;
    }
}

@end
