//
//  ViewController.h
//  p02-amaya
//
//  Created by Vanessa Amaya on 1/27/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import "GameboardView.h"
#import "ScoreView.h"
#import "TileView.h"

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    UISwipeGestureRecognizer *swipeLeft;
    UISwipeGestureRecognizer *swipeRight;
    UISwipeGestureRecognizer *swipeDown;
    UISwipeGestureRecognizer *swipeUp;
}

@property (nonatomic, strong) GameboardView *gameboardView;
@property (nonatomic, strong) ScoreView *scoreView;

@end
