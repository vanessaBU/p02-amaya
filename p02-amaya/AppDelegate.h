//
//  AppDelegate.h
//  p02-amaya
//
//  Created by Vanessa Amaya on 1/27/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

