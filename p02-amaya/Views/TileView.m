//
//  TileView.m
//  p02-amaya
//
//  Created by Vanessa Amaya on 1/29/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TileView.h"

@interface TileView ()

@end

@implementation TileView

// stackoverflow.com/qustions/17994473/how-do-i-create-a-custom-view-class-programmatically
- (id)initWithFrame:(CGRect)frame
          tileValue:(NSUInteger)value {
    
    self = [super initWithFrame:frame];
    if (self) {
        _position = CGPointMake(frame.origin.x, frame.origin.y);
        _width = frame.size.width;
        _numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(_position.x, _position.y, _width, _width)];
        [self setTileValue:value];
        _numberLabel.textColor = [UIColor whiteColor];
        _numberLabel.font = [UIFont fontWithName:@"Verdana" size:16];
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        // stackoverflow.com/questions/5155619/how-do-you-convert-an-nsuinteger-into-an-nsstring
        [self addSubview:_numberLabel];
    }
    return self;
}

- (void)setTileValue:(NSUInteger)value {
    switch (value) {
        case 2:
            _color = [UIColor colorWithRed:204./255. green:0./255. blue:0./255. alpha:0.4];
            break;
        case 4:
            _color = [UIColor colorWithRed:204./255. green:102./255. blue:0./255. alpha:0.4];
            break;
        case 8:
            _color = [UIColor colorWithRed:204./255. green:204./255. blue:0./255. alpha:0.4];
            break;
        case 16:
            _color = [UIColor colorWithRed:102./255. green:204./255. blue:0./255. alpha:0.4];
            break;
        case 32:
            _color = [UIColor colorWithRed:0./255. green:204./255. blue:0./255. alpha:0.4];
            break;
        case 64:
            _color = [UIColor colorWithRed:0./255. green:204./255. blue:102./255. alpha:0.4];
            break;
        case 128:
            _color = [UIColor colorWithRed:0./255. green:204./255. blue:204./255. alpha:0.4];
            break;
        case 256:
            _color = [UIColor colorWithRed:102./255. green:204./255. blue:0./255. alpha:0.4];
            break;
        case 512:
            _color = [UIColor colorWithRed:0./255. green:0./255. blue:204./255. alpha:0.4];
            break;
        case 1024:
            _color = [UIColor colorWithRed:102./255. green:0./255. blue:204./255. alpha:0.4];
            break;
        case 2048:
            _color = [UIColor colorWithRed:204./255. green:0./255. blue:204./255. alpha:0.4];
            break;
        case 0:
        default:
            _color = [UIColor whiteColor];
            break;
    }
    _numberLabel.backgroundColor = _color;
    _value = value;
    _numberLabel.text = [NSString stringWithFormat:@"%zd", value];
}

- (void)doubleTileValue {
    NSUInteger doubledValue = _value * 2;
    [self setTileValue:doubledValue];
}

@end
