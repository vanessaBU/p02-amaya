//
//  TileView.h
//  p02-amaya
//
//  Created by Vanessa Amaya on 1/29/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#ifndef TileView_h
#define TileView_h

#import <UIKit/UIKit.h>

@class TileView;

@interface TileView : UIView

@property (nonatomic) CGPoint position;
@property (nonatomic) NSInteger value;
@property (nonatomic) NSUInteger width;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UIColor *color;

- (id)initWithFrame:(CGRect)frame
          tileValue:(NSUInteger)value;
- (void)setTileValue:(NSUInteger)value;
- (void)doubleTileValue;

@end

#endif /* TileView_h */
