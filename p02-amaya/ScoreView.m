//
//  ScoreView.m
//  p02-amaya
//
//  Created by Vanessa Amaya on 2/6/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ScoreView.h"

@interface ScoreView ()

@end

@implementation ScoreView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _scoreLabel = [[UILabel alloc] initWithFrame:frame];
        _scoreLabel.backgroundColor = [UIColor lightGrayColor];
        _scoreLabel.textColor = [UIColor darkGrayColor];
        _scoreLabel.font = [UIFont fontWithName:@"Verdana" size:16];
        _scoreLabel.textAlignment = NSTextAlignmentCenter;
        // stackoverflow.com/questions/5155619/how-do-you-convert-an-nsuinteger-into-an-nsstring
        [self addSubview:_scoreLabel];
        [self setScore:0];
    }
    return self;
}

- (void)setScore:(NSUInteger)value {
    _scoreLabel.text = [NSString stringWithFormat:@"%zd", value];
}

@end