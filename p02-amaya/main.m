//
//  main.m
//  p02-amaya
//
//  Created by Vanessa Amaya on 1/27/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
