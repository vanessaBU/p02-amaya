//
//  ViewController.m
//  p02-amaya
//
//  Created by Vanessa Amaya on 1/27/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#import "ViewController.h"

// psellos.com/ocaml/example-app-slide24.html
// www.shmoopi.net/tutorials/slider-puzzle-ios-game-tutorial-2/

@interface ViewController()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //add gameboard to view
    _gameboardView = [[GameboardView alloc] initWithFrame:CGRectMake(0, 0, 225, 225) tileSize:50.0 padding:5.0];
    _gameboardView.center = self.view.center; // center gameboard in middle of screen
    [self.view addSubview:_gameboardView]; // show gameboard view
    
    _scoreView = [[ScoreView alloc] initWithFrame:CGRectMake(0, 30, 150, 30)];
    _scoreView.center = CGPointMake(CGRectGetMidX(self.view.bounds), _scoreView.center.y);
    [self.view addSubview:_scoreView];
    
    // add swipe gestures to view
    // stackoverflow.com/questions/24215117/how-to-recognize-swipe-in-all-4-directions
    // stackoverflow.com/questions/6326816/views-navigation-using-swipe-gesture
    // add swipe left gesture
    swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreenLeft:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    // add swipe right gesture
    swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreenRight:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    // add swipe up gesture
    swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreenUp:)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];
    // add swipe down gesture
    swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreenDown:)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDown];
}

- (void)swipedScreenLeft:(UIGestureRecognizer*)gesture {
    if (!_gameboardView.gameOver) {
        [_gameboardView shiftLeft];
        [_scoreView setScore:_gameboardView.score];
    }
    else {
        [_gameboardView resetTiles];
        [_scoreView setScore:0];
    }
}
- (void)swipedScreenRight:(UIGestureRecognizer*)gesture {
    if (!_gameboardView.gameOver) {
        [_gameboardView shiftRight];
        [_scoreView setScore:_gameboardView.score];
    }
    else {
        [_gameboardView resetTiles];
        [_scoreView setScore:0];
    }
}
- (void)swipedScreenUp:(UIGestureRecognizer*)gesture {
    if (!_gameboardView.gameOver) {
        [_gameboardView shiftUp];
        [_scoreView setScore:_gameboardView.score];
    }
    else {
        [_gameboardView resetTiles];
        [_scoreView setScore:0];
    }
}
- (void)swipedScreenDown:(UIGestureRecognizer*)gesture {
    if (!_gameboardView.gameOver) {
        [_gameboardView shiftDown];
        [_scoreView setScore:_gameboardView.score];
    }
    else {
        [_gameboardView resetTiles];
        [_scoreView setScore:0];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end