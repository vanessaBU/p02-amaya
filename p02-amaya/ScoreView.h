//
//  ScoreView.h
//  p02-amaya
//
//  Created by Vanessa Amaya on 2/6/17.
//  Copyright © 2017 Vanessa Amaya. All rights reserved.
//

#ifndef ScoreView_h
#define ScoreView_h

#import <UIKit/UIKit.h>

@class ScoreView;

@interface ScoreView : UIView

@property (nonatomic, strong) UILabel *scoreLabel;

- (void)setScore:(NSUInteger)value;

@end

#endif /* ScoreView_h */
